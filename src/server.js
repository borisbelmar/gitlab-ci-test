const express = require('express')

const app = express()

app.use(express.json())

app.get('/', (_req, res) => {
  res.send({ message: 'Hello!' })
})

app.set('PORT', process.env.PORT || 4000)

module.exports = app