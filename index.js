const app = require('./src/server')

app.listen(app.get('PORT'), err => {
  !err
    ? console.log('Services listening at port', app.get('PORT'))
    : console.log(err)
})